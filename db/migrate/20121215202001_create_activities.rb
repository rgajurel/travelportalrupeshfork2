class CreateActivities < ActiveRecord::Migration
  def change
    create_table :activities do |t|
      t.string :title
      t.text :description
      t.text :body
      t.boolean :inSeason
      t.float :price

      t.timestamps
    end
  end
end
