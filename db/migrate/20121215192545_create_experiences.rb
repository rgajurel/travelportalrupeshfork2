class CreateExperiences < ActiveRecord::Migration
  def change
    create_table :experiences do |t|
      t.string :title
      t.text :description
      t.text :body
      t.string :primary_image

      t.timestamps
    end
  end
end
