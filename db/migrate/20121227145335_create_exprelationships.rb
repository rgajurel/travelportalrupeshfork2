class CreateExprelationships < ActiveRecord::Migration
  def change
    create_table :exprelationships do |t|
      t.integer :experience_id
      t.integer :child_id

      t.timestamps
    end
    add_index :exprelationships, :experience_id
    add_index :exprelationships, :child_id
    add_index :exprelationships, [:experience_id, :child_id], unique:true
  end
end
