require 'test_helper'

class ExperienceTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  test "experience attributes must not be empty" do
    experience = Experience.new
    assert experience.invalid?
    assert experience.errors[:title].any?
    assert experience.errors[:description].any?
  end

  test "experience cannot have invalid image link" do
    experience = Experience.new(title: "Trekking", description: "Simple description", primary_image: "xxx.jpt")
    assert experience.invalid?
    assert_equal ["must be a URL for GIF, JPG or PNG image."], experience.errors[:primary_image]
  end
end
