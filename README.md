# Every journey begins with a single step, this is the step, for the Mountains

## Installation

* For the project to your own space
* git clone git@bitbucket.org:<username>/travelportal.git
* cd travelportal.git
* rails db:rake
* rails server

Then in your browser go to http://localhost:3000 and you should see the basic rails app setup

*The information below can be ignored. It is here for reference purposes only.*

## Deployment on heroku

Make sure you have added heroku as a remote to your git repository.

    git remote add heroku <heroku-remote-url>
    git push heroku master

The above push command runs all necessary things like bundle install etc on heroku

    git run rake db:migrate

Runs all migrations on heroku

Finally the application should be accessible at http://vast-journey-1966.herokuapp.com/

Instruction for rails deployment on heroku is given at https://devcenter.heroku.com/articles/rails3

