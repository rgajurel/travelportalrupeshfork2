class Exprelationship < ActiveRecord::Base
  attr_accessible :child_id, :experience_id
  belongs_to :parent_experience, class_name: "Experience"
  belongs_to :child_experience, class_name: "Experience"

  validates :parent_experience, presence: true
  validates :child_experience, presence: true
end
