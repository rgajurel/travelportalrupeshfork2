class Experience < ActiveRecord::Base
  attr_accessible :title, :body, :description, :primary_image

  #has_many :exprelationships, foreign_key:"experience_id", dependent: :destroy
 # has_many :child_experiences, through: :exprelationships, source: :child_experience
  validates :title, :description, :primary_image, presence: true
  validates :title, uniqueness: true
  validates :primary_image, allow_blank: true, format: {
      with: %r{\.(gif|jpg|png)\Z}i,
      message: 'must be a URL for GIF, JPG or PNG image.'}


  def addChild!(other_experience)
    exprelationships.create!(child_id: other_experience.id)
  end
end
